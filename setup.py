"""
=============
DOVICO API
=============

A Python wrapper around the DOVICO API

"""
from setuptools import setup

setup(
    name='dovicoapi',
    version='0.0.1',
    packages=['dovicoapi'],
    url='',
    license='BSD',
    author='Patrick Golec',
    author_email='pgolec+pypi@gmail.com',
    description='Python library simplifying working with the DOVICO API',
    long_description=__doc__,

    # run-time dependencies
    install_requires=[
        'PyYAML',
        'requests',
        'bigpy'
    ],
)
