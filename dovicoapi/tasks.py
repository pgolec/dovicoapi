"""
Tasks API.
"""
from dovicoapi.base import DovicoResourceHandlerApi


class TasksApi(DovicoResourceHandlerApi):
    """
    Implements DOVICO API resource handler for Tasks.
    """
