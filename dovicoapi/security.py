"""
Security Groups API.
"""
from dovicoapi.base import DovicoResourceHandlerApi


class SecurityGroupsApi(DovicoResourceHandlerApi):
    """
    Implements DOVICO API resource handler for Security Groups.
    """
