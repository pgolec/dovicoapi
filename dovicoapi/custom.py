"""
Custom Terminology and Custom Field Templates API.
"""
from dovicoapi.base import DovicoResourceHandlerApi


class CustomTerminologyApi(DovicoResourceHandlerApi):
    """
    Implements DOVICO API resource handler for Custom Terminology.
    """


class CustomFieldTemplatesApi(DovicoResourceHandlerApi):
    """
    Implements DOVICO API resource handler for Custom Field Templates.
    """
