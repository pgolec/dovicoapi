"""
Clients API.
"""
from dovicoapi.base import DovicoResourceHandlerApi, DovicoEntityDescriptor


class ClientsApi(DovicoResourceHandlerApi):
    """
    Implements DOVICO API resource handler for Clients.
    """
    _ENTITY_DESCRIPTOR = DovicoEntityDescriptor('Client')
