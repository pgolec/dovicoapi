"""
Assignments API.
"""
from dovicoapi.base import DovicoResourceHandlerApi


class AssignmentsApi(DovicoResourceHandlerApi):
    """
    Implements DOVICO API resource handler for Assignments.
    """
