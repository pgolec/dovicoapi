"""
Employees API.
"""
from dovicoapi.base import DovicoResourceHandlerApi, DovicoEntityDescriptor


class EmployeesApi(DovicoResourceHandlerApi):
    """
    Implements DOVICO API resource handler for Employees.
    """
    _ENTITY_DESCRIPTOR = DovicoEntityDescriptor('Employee')

