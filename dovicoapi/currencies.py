"""
Currencies and Currency Rate API.
"""
from dovicoapi.base import DovicoResourceHandlerApi, DovicoEntityDescriptor


class CurrenciesApi(DovicoResourceHandlerApi):
    """
    Implements DOVICO API resource handler for Currencies.
    """
    _ENTITY_DESCRIPTOR = DovicoEntityDescriptor('Currency', 'Currencies')


class CurrencyRateApi(DovicoResourceHandlerApi):
    """
    Implements DOVICO API resource handler for Currency Rate.
    """
    _ENTITY_DESCRIPTOR = DovicoEntityDescriptor('Currency Rate', 'CurrencyRates')
