"""
Expense Categories and Expense Entries API.
"""
from dovicoapi.base import DovicoResourceHandlerApi


class ExpenseEntriesApi(DovicoResourceHandlerApi):
    """
    Implements DOVICO API resource handler for Expense Entries.
    """


class ExpenseCategoriesApi(DovicoResourceHandlerApi):
    """
    Implements DOVICO API resource handler for Expense Categories.
    """
