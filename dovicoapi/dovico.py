"""
DOVICO client
"""
from dovicoapi.assignments import AssignmentsApi
from dovicoapi.clients import ClientsApi
from dovicoapi.currencies import CurrenciesApi, CurrencyRateApi
from dovicoapi.custom import CustomFieldTemplatesApi, CustomTerminologyApi
from dovicoapi.employees import EmployeesApi
from dovicoapi.expenses import ExpenseCategoriesApi, ExpenseEntriesApi
from dovicoapi.projects import ProjectsApi
from dovicoapi.security import SecurityGroupsApi
from dovicoapi.sheets import SheetsApi
from dovicoapi.tasks import TasksApi
from dovicoapi.teams import TeamsApi
from dovicoapi.timeentries import TimeEntriesApi


class Dovico(object):
    """
    A convenience class for accessing various DOVICO resources.
    """
    def __init__(self, consumer_secret, data_access_token, api_version=None, api_base_url=None):
        self.assignments = AssignmentsApi(consumer_secret, data_access_token, api_version, api_base_url)
        self.clients = ClientsApi(consumer_secret, data_access_token, api_version, api_base_url)
        self.currencies = CurrenciesApi(consumer_secret, data_access_token, api_version, api_base_url)
        self.currency_rate = CurrencyRateApi(consumer_secret, data_access_token, api_version, api_base_url)
        self.custom_field_templates = CustomFieldTemplatesApi(
                consumer_secret, data_access_token, api_version, api_base_url
        )
        self.custom_terminology = CustomTerminologyApi(consumer_secret, data_access_token, api_version, api_base_url)
        self.employees = EmployeesApi(consumer_secret, data_access_token, api_version, api_base_url)
        self.expense_categories = ExpenseCategoriesApi(consumer_secret, data_access_token, api_version, api_base_url)
        self.expense_entries = ExpenseEntriesApi(consumer_secret, data_access_token, api_version, api_base_url)
        self.projects = ProjectsApi(consumer_secret, data_access_token, api_version, api_base_url)
        self.security_groups = SecurityGroupsApi(consumer_secret, data_access_token, api_version, api_base_url)
        self.sheets = SheetsApi(consumer_secret, data_access_token, api_version, api_base_url)
        self.tasks = TasksApi(consumer_secret, data_access_token, api_version, api_base_url)
        self.teams = TeamsApi(consumer_secret, data_access_token, api_version, api_base_url)
        self.time_entries = TimeEntriesApi(consumer_secret, data_access_token, api_version, api_base_url)

