"""
Sheets API.
"""
from dovicoapi.base import DovicoResourceHandlerApi


class SheetsApi(DovicoResourceHandlerApi):
    """
    Implements DOVICO API resource handler for Sheets.
    """
