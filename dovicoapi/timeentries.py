"""
Time Entries API.
"""
from dovicoapi.base import DovicoResourceHandlerApi, DovicoEntityDescriptor


class TimeEntriesApi(DovicoResourceHandlerApi):
    """
    Implements DOVICO API resource handler for Time Entries.
    """
    _ENTITY_DESCRIPTOR = DovicoEntityDescriptor('TimeEntry', 'TimeEntries')

    def find_entries_in_range(self, start_date, end_date=None, query_filter=None):
        """
        Returns all time entries between the specified dates, optionally further filtered with the
        specified filter query string.
        :param start_date: the start date.
        :param end_date: the end date. If it is None, we use current date.
        :param query_filter: the optional query filter.
        :return: all time entries matching the specified criteria as a list if `DotDict` objects.
        """
        params = self._build_range_and_filter_params(start_date, end_date, query_filter)
        return self.find_all(**params)

    def find_entries_in_status(self, status, start_date, end_date=None, query_filter=None):
        """
        Returns all time entries in the specified status and between the specified dates,
        optionally further filtered with the specified filter query string.
        :param status: the status of the (e.g. 'A' for approved or 'U' for under review)
        :param start_date: the start date.
        :param end_date: the end date. If it is None, we use current date.
        :param query_filter: the optional query filter.
        :return: all time entries matching the specified criteria as a list if `DotDict` objects.
        """
        params = self._build_range_and_filter_params(start_date, end_date, query_filter)
        params, headers = self._init_params_and_headers(params)
        return self._load_collection(self._make_url('Status/{0}/'.format(status)), params=params, headers=headers)

    def find_entries_for_employee(self, employee_id, start_date, end_date=None, query_filter=None):
        """
        Returns all time entries for the specified employee and between the specified dates,
        optionally further filtered with the specified filter query string.
        :param employee_id: the id of an employee for whom we are retrieving the time entries.
        :param start_date: the start date.
        :param end_date: the end date. If it is None, we use current date.
        :param query_filter: the optional query filter.
        :return: all time entries matching the specified criteria as a list if `DotDict` objects.
        """
        params = self._build_range_and_filter_params(start_date, end_date, query_filter)
        params, headers = self._init_params_and_headers(params)
        return self._load_collection(
                self._make_url('Employee/{0}/'.format(employee_id)),
                params=params,
                headers=headers
        )

    def find_entries_for_project(self, project_id, start_date, end_date=None, query_filter=None):
        """
        Returns all time entries for the specified project and between the specified dates,
        optionally further filtered with the specified filter query string.
        :param project_id: the id of the project.
        :param start_date: the start date.
        :param end_date: the end date. If it is None, we use current date.
        :param query_filter: the optional query filter.
        :return: all time entries matching the specified criteria as a list if `DotDict` objects.
        """
        params = self._build_range_and_filter_params(start_date, end_date, query_filter)
        params, headers = self._init_params_and_headers(params)
        return self._load_collection(
                self._make_url('Project/{0}/'.format(project_id)),
                params=params,
                headers=headers
        )

    def find_entries_for_sheet(self, sheet_id, start_date, end_date=None, query_filter=None):
        """
        Returns all time entries for the specified sheet and between the specified dates,
        optionally further filtered with the specified filter query string.
        :param sheet_id: the id of the sheet.
        :param start_date: the start date.
        :param end_date: the end date. If it is None, we use current date.
        :param query_filter: the optional query filter.
        :return: all time entries matching the specified criteria as a list if `DotDict` objects.
        """
        params = self._build_range_and_filter_params(start_date, end_date, query_filter)
        params, headers = self._init_params_and_headers(params)
        return self._load_collection(
                self._make_url('Sheet/{0}/'.format(sheet_id)),
                params=params,
                headers=headers
        )
