"""
Common classes and functions used throughout the package
"""
import requests
import timeit
import time
from datetime import date, datetime
from bigpy import DotDict, ensure_not_blank


class DovicoError(Exception):
    """
    Base exception class for different exceptions raised in this package.
    """
    pass


class DovicoApiError(DovicoError):
    """
    Root of the exception hierarchy for communicating errors when interacting with DOVICO API.
    It carries the HTTP status code along with the message.
    """
    def __init__(self, message, status_code):
        super(DovicoApiError, self).__init__(message)
        self.status_code = status_code


class DovicoUnauthorizedError(DovicoApiError):
    """
    Raised in response to HTTP status 401
    """
    DEFAULT_MESSAGE = 'Unauthorized: Access is denied due to invalid credentials'

    def __init__(self, message=DEFAULT_MESSAGE, status_code=401):
        super(DovicoUnauthorizedError, self).__init__(message, status_code)


class DovicoEntityNotFoundError(DovicoApiError):
    """
    Raised in response to HTTP status 400 (Not Found)
    """
    DEFAULT_MESSAGE = 'Entity ID [{0}] not found'

    def __init__(self, entity_id, message=DEFAULT_MESSAGE, status_code=400):
        super(DovicoEntityNotFoundError, self).__init__(message.format(entity_id), status_code)


class DovicoApi(object):
    """
    Base class for all classes using DOVICO API.
    """

    # The root URI for REST API calls
    API_URL = 'https://api.dovico.com/'

    # Default API version
    API_VERSION = '5'

    def __init__(self, consumer_secret, data_access_token, api_version=None, api_base_url=None):
        """
        Initializes the API handler. In particular, it provides the credentials required
        to use DOVICO REST-style API. See http://apideveloper.dovico.com/Getting+Started for details.

        :param consumer_secret: The Company account specific token is required for API authentication.
        :param data_access_token: Either a special Company account specific data access token that has
            full access to All Data in the application or a Company account user access token that
            grants an employee access to only the data permitted by his or her security settings as
            defined in the Dovico software.
        :param api_version: The version of the DOVICO API to use.Defaults to `API_VERSION`
        :param api_base_url: The base URL of all DOVICO API requests. Defaults to `API_URL`
        """
        api_version = str(api_version).strip() if api_version else ''
        api_base_url = api_base_url.strip() if api_base_url else ''

        self.consumer_secret = ensure_not_blank(consumer_secret)
        self.data_access_token = ensure_not_blank(data_access_token)
        self.api_version = api_version if api_version else DovicoResourceHandlerApi.API_VERSION
        self.api_base_url = api_base_url if api_base_url else DovicoResourceHandlerApi.API_URL

        if not self.api_base_url.endswith('/'):
            self.api_base_url += '/'

    @staticmethod
    def _combine_path(base_url, path):
        url = base_url + path if path else base_url
        if not url.endswith('/'):
            url += '/'
        return url

    @staticmethod
    def _extract_error_description(response, default_message=None):
        try:
            parsed = response.json()
            if 'Description' in parsed and parsed['Description']:
                return parsed['Description']
        except ValueError:
            return default_message if default_message else 'Unknown error occurred'

    @staticmethod
    def _handle_standard_errors(response):
        if response.status_code == 401:
            raise DovicoUnauthorizedError()
        else:
            message = DovicoApi._extract_error_description(response)
            raise DovicoApiError(message, response.status_code)

    @property
    def _base_url(self):
        return self.api_base_url

    def _make_url(self, path):
        return self._combine_path(self._base_url, path)

    def _get(self, path=None, params=None, headers=None):
        params, headers = self._init_params_and_headers(params, headers, has_content=False)
        return requests.get(self._make_url(path), params=params, headers=headers)

    def _put(self, data, path=None, headers=None):
        # noinspection PyTypeChecker
        params, headers = self._init_params_and_headers(None, headers, has_content=True)
        return requests.put(self._make_url(path), params=params, headers=headers, data=data)

    def _post(self, data, path=None, headers=None):
        # noinspection PyTypeChecker
        params, headers = self._init_params_and_headers(None, headers, has_content=True)
        return requests.post(self._make_url(path), params=params, headers=headers, data=data)

    def _init_params_and_headers(self, params, headers=None, has_content=False):
        params = dict(params) if params else {}
        params['version'] = self.api_version

        headers = dict(headers) if headers else {}
        headers['Authorization'] = 'WRAP access_token="client={0}&user_token={1}"'.format(
            self.consumer_secret, self.data_access_token
        )
        headers['Accept'] = 'application/json'
        if has_content:
            headers['Content-Type'] = 'application/json'
        return params, headers


class DovicoEntityDescriptor(object):
    """
    Represents Dovico entity (resource) metadata - such as the name, collection root, etc.
    """
    def __init__(self, name, collection_root=None, collection_path=None):
        self.name = ensure_not_blank(name)
        self.collection_root = collection_root if collection_root else self.name + 's'
        self.collection_path = collection_path if collection_path else self.collection_root


class DovicoResourceHandlerApi(DovicoApi):
    """
    Base class for all DOVICO API resource-specific API classes.
    """
    _ENTITY_DESCRIPTOR = None

    __DATE_FORMAT = '%Y-%m-%d'
    __DEFAULT_MAX_REQUEST_PER_INTERVAL = 5
    __DEFAULT_INTERVAL_IN_MILLISECONDS = 1000
    __NEXT_PAGE_URI = 'NextPageURI'

    FILTER_ARG = 'query_filter'
    DATE_RANGE_ARG = 'date_range'

    def __init__(self, consumer_secret, data_access_token, api_version=None, api_base_url=None):
        super(DovicoResourceHandlerApi, self).__init__(consumer_secret, data_access_token, api_version, api_base_url)
        self._max_requests_per_interval = self.__DEFAULT_MAX_REQUEST_PER_INTERVAL
        self._interval = self.__DEFAULT_INTERVAL_IN_MILLISECONDS

    @property
    def _entity_name(self):
        return self._ENTITY_DESCRIPTOR.name

    @property
    def _collection_root(self):
        return self._ENTITY_DESCRIPTOR.collection_root

    @property
    def _collection_path(self):
        return self._ENTITY_DESCRIPTOR.collection_path

    @property
    def _base_url(self):
        return self._combine_path(self.api_base_url, self._collection_path)

    @property
    def _entity_not_found_message(self):
        return self._entity_name + ' ID [{0}] not found'

    def _extract_params(self, **kwargs):
        """
        Extracts additional query parameters from the keyword arguments. This method can be overridden by the
        derived class to provide a specialized behaviour. The base implementation looks for the filter string
        as well as the date range.

        :param kwargs: keyword arguments.
        :return: a dictionary with the properly formatted and named query parameters.
        """
        params = {}
        if self.FILTER_ARG in kwargs:
            params['$filter'] = kwargs[self.FILTER_ARG]

        if self.DATE_RANGE_ARG in kwargs:
            # date range is represented as a list or tuple of two
            # dates or strings
            start_date = kwargs[self.DATE_RANGE_ARG][0]
            end_date = kwargs[self.DATE_RANGE_ARG][1]
            date_range = '{0} {1}'.format(
                date.strftime(start_date, self.__DATE_FORMAT) if isinstance(start_date, date) else start_date,
                date.strftime(end_date, self.__DATE_FORMAT) if isinstance(end_date, date) else end_date
            )
            params['daterange'] = date_range

        return params

    def _build_range_and_filter_params(self, start_date, end_date, query_filter):
        if not end_date:
            end_date = datetime.now()
        params = {'date_range': (start_date, end_date)}
        if query_filter:
            params['query_filter'] = query_filter
        return self._extract_params(**params)

    def _load_collection(self, url, params, headers):
        """
        Internal method responsible for loading all entities in the collection, even if the results are
        returned one page at a time. This is done by recursively calling the API, using the URL specified
        in the NextPageURI attribute of the JSON result. As well, DOVICO API claims to limit API requests
        to 5 calls per second and 1000 results returned per call. This method attempts to throttle rapid
        series of request to ensure that we stay within the API imposed limits.

        :param url: the API URL to call.
        :param params: any query parameters to pass to the first API call.
        :param headers: any HTTP headers to attach to each call.
        :return: all entities in the collection.
        """
        load_more = True
        requests_per_interval = 1
        result_list = list()
        start_time = timeit.default_timer()
        response = requests.get(url, params=params, headers=headers)

        while load_more:
            if response.status_code == 200:
                result = response.json()
                if self._collection_root in result:
                    result_list.extend([DotDict(x) for x in result[self._collection_root]])
                if self.__NEXT_PAGE_URI in result:
                    url = result[self.__NEXT_PAGE_URI]
                    load_more = url and url.upper() != 'N/A'
                    if load_more:
                        # throttle to ensure we're not exceeding the maximum
                        # number of API requests per the specified interval
                        requests_per_interval += 1
                        if requests_per_interval == self._max_requests_per_interval:
                            time_elapsed = timeit.default_timer() - start_time
                            if time_elapsed * 1000 < self._interval:
                                the_diff = self._interval - time_elapsed * 1000
                                time.sleep((the_diff + 100) / float(1000))
                                requests_per_interval = 1
                                start_time = timeit.default_timer()
                        response = requests.get(url, headers=headers)
                else:
                    load_more = False
            else:
                self._handle_standard_errors(response)

        return result_list

    def _load_single(self, path):
        response = self._get(path)
        if response.status_code == 200:
            return DotDict(response.json())
        else:
            self._handle_standard_errors(response)

    def find_all(self, **kwargs):
        params = self._extract_params(**kwargs)
        params, headers = self._init_params_and_headers(params)
        return self._load_collection(self._base_url, params=params, headers=headers)

    def find_by_id(self, entity_id):
        entity_id = ensure_not_blank(str(entity_id) if entity_id else None)
        response = self._get(entity_id)
        if response.status_code == 200:
            result = response.json()
            if self._collection_root in result and result[self._collection_root]:
                return DotDict(result[self._collection_root][0])
            else:
                raise DovicoEntityNotFoundError(entity_id, self._entity_not_found_message)
        else:
            self._handle_standard_errors(response)
