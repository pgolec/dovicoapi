"""
Projects API.
"""
from dovicoapi.base import DovicoResourceHandlerApi, DovicoEntityDescriptor


class ProjectsApi(DovicoResourceHandlerApi):
    """
    Implements DOVICO API resource handler for Projects.
    """
    _ENTITY_DESCRIPTOR = DovicoEntityDescriptor('Project')

    def find_active_projects(self):
        return self.find_all(query_filter='Status eq A')

    def get_project_statistics(self, project_id):
        return self._load_single('{0}/Statistics'.format(project_id))

    def get_project_expense_totals(self, project_id):
        return self._load_single('{0}/ExpenseTotals'.format(project_id))
