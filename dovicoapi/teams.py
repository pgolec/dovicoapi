"""
Teams API.
"""
from dovicoapi.base import DovicoResourceHandlerApi


class TeamsApi(DovicoResourceHandlerApi):
    """
    Implements DOVICO API resource handler for Teams.
    """
