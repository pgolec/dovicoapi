"""
Unit tests for the Clients
"""
# noinspection PyUnresolvedReferences
from tests.test_helpers import credentials
from dovicoapi.clients import ClientsApi


# noinspection PyProtectedMember,PyShadowingNames
def test_clients_entity_descriptor(credentials):
    api = ClientsApi(credentials.consumer_secret, credentials.data_access_token)
    assert api._entity_name == 'Client'
    assert api._collection_root == 'Clients'
    assert api._collection_path == 'Clients'
    assert api._entity_not_found_message == 'Client ID [{0}] not found'
