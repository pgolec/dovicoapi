"""
Unit tests for the Time Entries
"""
# noinspection PyUnresolvedReferences
from tests.test_helpers import credentials
from dovicoapi.timeentries import TimeEntriesApi


# noinspection PyProtectedMember,PyShadowingNames
def test_time_entries_entity_descriptor(credentials):
    api = TimeEntriesApi(credentials.consumer_secret, credentials.data_access_token)
    assert api._entity_name == 'TimeEntry'
    assert api._collection_root == 'TimeEntries'
    assert api._collection_path == 'TimeEntries'
    assert api._entity_not_found_message == 'TimeEntry ID [{0}] not found'
