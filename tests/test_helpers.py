"""
Unit test helpers
"""
import os
import yaml
import uuid
import pytest
import httpretty

CREDENTIALS_DIR = '.dovicoapitests'
CREDENTIALS_FILE_NAME = 'credentials.yaml'


# noinspection PyShadowingNames
class CredentialHelper(object):
    """
    Loads DOVICO API access tokens from a credentials file called credentials.yaml located in .dovicoapitests
    subdirectory of user's home directory, e.g. for user "pgolec" this file would be:

    /home/pgolec/.dovicoapitests/credentials.yaml

    Its contents look like this:

        consumer_secret: '<string value>'
        data_access_token: '<string value>'

    """
    def __init__(self, credentials_file_name=CREDENTIALS_FILE_NAME, credentials_dir=CREDENTIALS_DIR):

        home = os.path.expanduser("~")
        credentials_file = os.path.join(home, credentials_dir, credentials_file_name)

        with open(credentials_file, 'r') as f:
            credentials = yaml.load(f)

        self.consumer_secret = credentials['consumer_secret']
        self.data_access_token = credentials['data_access_token']


# noinspection PyShadowingNames
@pytest.fixture
def file_name_and_credentials(request):

    home = os.path.expanduser("~")
    credentials_dir = os.path.join(home, '.' + str(uuid.uuid4()))

    if not os.path.exists(credentials_dir):
        os.mkdir(credentials_dir)

    file_name = str(uuid.uuid4())
    file_path = os.path.join(credentials_dir, file_name)
    consumer_secret = str(uuid.uuid4())
    data_access_token = str(uuid.uuid4())

    credentials = [
        'consumer_secret: {0}\n'.format(consumer_secret),
        'data_access_token: {0}\n'.format(data_access_token)
    ]

    with open(file_path, 'w') as f:
        f.writelines(credentials)

    def fin():
        os.unlink(file_path)
        os.rmdir(credentials_dir)

    request.addfinalizer(fin)

    return file_name, credentials_dir, consumer_secret, data_access_token


# noinspection PyShadowingNames
def test_credential_helper(file_name_and_credentials):

    file_name = file_name_and_credentials[0]
    credentials_dir = file_name_and_credentials[1]
    consumer_secret = file_name_and_credentials[2]
    data_access_token = file_name_and_credentials[3]

    ch = CredentialHelper(credentials_file_name=file_name, credentials_dir=credentials_dir)
    assert ch.consumer_secret == consumer_secret
    assert ch.data_access_token == data_access_token


@pytest.fixture(scope='session')
def credentials():
    return CredentialHelper()


# noinspection PyUnusedLocal
class HttpPrettyHelper(object):
    """
    HTTPretty context manager to be used instead of the HTTPretty decorator. The decorator doesn't seem
    to work with the pytest fixtures - looks more like a pytest problem...
    """
    def __enter__(self):
        httpretty.reset()
        httpretty.enable()

    def __exit__(self, exc_type, exc_val, exc_tb):
        httpretty.disable()
        httpretty.reset()



