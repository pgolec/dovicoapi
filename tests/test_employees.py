"""
Unit tests for the Employees
"""
import httpretty
import pytest
import json
import timeit
import random

from dovicoapi.base import DovicoApiError, DovicoEntityNotFoundError
from dovicoapi.employees import EmployeesApi
# noinspection PyUnresolvedReferences
from tests.test_helpers import credentials, HttpPrettyHelper

# how many employee records in the "long" response?
LONG_RESPONSE_EMPLOYEE_COUNT = 1500


def __build_find_by_id_response(find_by_all_response):
    """
    Creates a single result JSON response from a JSON string representing a collection.
    :param find_by_all_response: a JSON string representing response to a `find-all` API call.
    :return: a single result JSON response
    """
    res = json.loads(find_by_all_response)
    res['Employees'] = res['Employees'][:1]
    del res['PrevPageURI']
    del res['NextPageURI']
    return json.dumps(res, separators=(',', ':'))


def __build_long_employee_response(response_template, count=LONG_RESPONSE_EMPLOYEE_COUNT):
    """
    Using an existing JSON response as a template, creates JSON response with a different number of employees,
    defined by the `count` argument - presumably a reasonably large value.
    :param response_template: a JSON string representing a response to `find-all` API call.
    :return: an "extrapolated" JSON string representing a response to `find-all` API call with a different
        (presumably larger) number of employees.
    """
    new_response = {'Employees': [], 'PrevPageURI': 'N/A', 'NextPageURI': 'N/A'}
    res = json.loads(response_template)
    i = 0
    original_count = len(res['Employees'])

    # we want to ensure we are, in fact, "extending" the response
    assert original_count < count

    for employee_number in xrange(0, count):
        if i >= original_count:
            i = 0
        e = dict(res['Employees'][i])
        e['ID'] = str(100 + employee_number)
        e['FirstName'] = 'Lester{0}'.format(employee_number)
        e['LastName'] = 'Tester{0}'.format(employee_number)
        e['Email'] = 'lester{0}.tester{0}@test.com'.format(employee_number)

        new_response['Employees'].append(e)
        i += 1

    return json.dumps(new_response, separators=(',', ':'))


def __build_long_employee_paged_response(response_template, count=LONG_RESPONSE_EMPLOYEE_COUNT, page_size=100):
    """
    Using an existing JSON response as a template, creates JSON "pages" with 100 employees each for
    a different, larger number of employees, specified by the `count` argument.
    :param response_template: a JSON string representing a response to a `find-all` API call.
    :return: an array of "extrapolated" JSON string representing a response to a `find-all` API call
        with a presumably larger number (`count`) of employees.
    """
    long_response = json.loads(__build_long_employee_response(response_template, count=count))
    pages = []
    employees_per_page = 0
    current_page = None

    def page_url(which, x):
        return 'https://api.dovico.com/Employees/?{0}={1}&version=5'.format(which, x)

    for employee_number in xrange(0, count):
        if employees_per_page > page_size - 1:
            employees_per_page = 0
        if employees_per_page == 0:
            current_page = {'Employees': [], 'PrevPageURI': 'N/A', 'NextPageURI': 'N/A'}
            pages.append(current_page)
            if employee_number > page_size - 1:
                current_page['PrevPageURI'] = page_url('prev', employee_number - page_size)
            if LONG_RESPONSE_EMPLOYEE_COUNT - employee_number > page_size:
                current_page['NextPageURI'] = page_url('next', employee_number + page_size)

        current_page['Employees'].append(long_response['Employees'][employee_number])
        employees_per_page += 1

    # turn each page into JSON string
    return [json.dumps(page, separators=(',', ':')) for page in pages]


def test_build_find_by_id_response():
    """
    Tests our internal routine for creating a single result response from a collection
    """
    resp = __build_find_by_id_response(FIND_ALL_RESPONSE)
    assert resp.startswith('{"Employees":[{')
    assert resp.endswith('}]}')

    parsed = json.loads(resp)
    assert 'Employees' in parsed
    assert len(parsed['Employees']) == 1
    assert parsed['Employees'][0]['FirstName'] == 'Lester1'
    assert parsed['Employees'][0]['LastName'] == 'Tester1'
    assert parsed['Employees'][0]['ID'] == '114'

    assert 'PrevPageURI' not in parsed
    assert 'NextPageURI' not in parsed


def test_build_long_employee_response():
    """
    Tests our internal routine for creating a 5000 employee response from a small collection
    """
    test_count = 5000
    resp = __build_long_employee_response(FIND_ALL_RESPONSE, count=test_count)
    assert resp.startswith('{"Employees":[{')

    parsed = json.loads(resp)
    assert 'Employees' in parsed
    assert 'PrevPageURI' in parsed
    assert 'NextPageURI' in parsed

    assert len(parsed['Employees']) == test_count

    assert parsed['Employees'][435]['FirstName'] == 'Lester435'
    assert parsed['Employees'][435]['LastName'] == 'Tester435'
    assert parsed['Employees'][435]['Email'] == 'lester435.tester435@test.com'
    assert parsed['Employees'][435]['ID'] == '535'

    assert parsed['Employees'][4358]['FirstName'] == 'Lester4358'
    assert parsed['Employees'][4358]['LastName'] == 'Tester4358'
    assert parsed['Employees'][4358]['Email'] == 'lester4358.tester4358@test.com'
    assert parsed['Employees'][4358]['ID'] == '4458'

    assert parsed['PrevPageURI'] == 'N/A'
    assert parsed['NextPageURI'] == 'N/A'


def test_build_long_employee_paged_response():
    """
    Tests our internal routine by creating a 50 pages @ 100 employees each response from a small collection
    """
    resp_list = __build_long_employee_paged_response(FIND_ALL_RESPONSE, count=5000, page_size=100)

    assert len(resp_list) == 50

    # edge case: first "page"
    parsed = json.loads(resp_list[0])

    assert 'Employees' in parsed
    assert 'PrevPageURI' in parsed
    assert 'NextPageURI' in parsed

    assert len(parsed['Employees']) == 100
    assert parsed['PrevPageURI'] == 'N/A'
    assert parsed['NextPageURI'] == 'https://api.dovico.com/Employees/?next=100&version=5'

    assert parsed['Employees'][43]['FirstName'] == 'Lester43'
    assert parsed['Employees'][43]['LastName'] == 'Tester43'
    assert parsed['Employees'][43]['Email'] == 'lester43.tester43@test.com'
    assert parsed['Employees'][43]['ID'] == '143'

    # sample page 7
    parsed = json.loads(resp_list[7])

    assert 'Employees' in parsed
    assert 'PrevPageURI' in parsed
    assert 'NextPageURI' in parsed

    assert len(parsed['Employees']) == 100
    assert parsed['PrevPageURI'] == 'https://api.dovico.com/Employees/?prev=600&version=5'
    assert parsed['NextPageURI'] == 'https://api.dovico.com/Employees/?next=800&version=5'

    assert parsed['Employees'][43]['FirstName'] == 'Lester743'
    assert parsed['Employees'][43]['LastName'] == 'Tester743'
    assert parsed['Employees'][43]['Email'] == 'lester743.tester743@test.com'
    assert parsed['Employees'][43]['ID'] == '843'

    # edge case: last "page"
    parsed = json.loads(resp_list[49])

    assert 'Employees' in parsed
    assert 'PrevPageURI' in parsed
    assert 'NextPageURI' in parsed

    assert len(parsed['Employees']) == 100
    assert parsed['PrevPageURI'] == 'https://api.dovico.com/Employees/?prev=4800&version=5'
    assert parsed['NextPageURI'] == 'N/A'

    assert parsed['Employees'][43]['FirstName'] == 'Lester4943'
    assert parsed['Employees'][43]['LastName'] == 'Tester4943'
    assert parsed['Employees'][43]['Email'] == 'lester4943.tester4943@test.com'
    assert parsed['Employees'][43]['ID'] == '5043'


# noinspection PyShadowingNames
@pytest.fixture(scope='module')
def api(credentials):
    return EmployeesApi(credentials.consumer_secret, credentials.data_access_token)


# noinspection PyProtectedMember,PyShadowingNames
def test_employees_entity_descriptor(api):
    assert api._entity_name == 'Employee'
    assert api._collection_root == 'Employees'
    assert api._collection_path == 'Employees'
    assert api._entity_not_found_message == 'Employee ID [{0}] not found'


# noinspection PyShadowingNames
def test_employees_find_all_success(api):
    with HttpPrettyHelper():
        httpretty.register_uri(
                httpretty.GET,
                'https://api.dovico.com/Employees/',
                body=FIND_ALL_RESPONSE,
                content_type='application/json; charset=utf-8'
        )

        employees = api.find_all()

        assert employees
        assert len(employees) == 5
        assert employees[0].ID == '114'
        assert employees[0].FirstName == 'Lester1'
        assert employees[0].LastName == 'Tester1'

        assert httpretty.last_request().headers['Accept'] == 'application/json'
        assert len(httpretty.last_request().querystring['version']) == 1
        assert int(httpretty.last_request().querystring['version'][0]) == 5


# noinspection PyShadowingNames
def test_employees_find_all_paged(api):
    """
    Tests our API with a long, paged response. The test is designed to also verify throttling mechanism
    that attempts to stay within DOVICO API requests limits (which may or may not actually exist)
    :param api: the `EmployeesApi` instance
    """
    with HttpPrettyHelper():
        # get pages of the paged response
        pages = __build_long_employee_paged_response(FIND_ALL_RESPONSE, 1500, 100)
        # ensure we have the expected number of pages
        assert len(pages) == 15

        # our request callback
        # noinspection PyUnusedLocal
        def request_callback(request, uri, headers):
            # noinspection SpellCheckingInspection
            headers['content_type'] = 'application/json; charset=utf-8'

            body = pages[0]
            if 'next' in request.querystring:
                next_page = int(request.querystring['next'][0])
                body = pages[next_page/100]
            return 200, headers, body

        # register URL with our request callback
        httpretty.register_uri(
            httpretty.GET,
            'https://api.dovico.com/Employees/',
            body=request_callback
        )

        # DOVICO API claims to limit API requests to 5 calls per second and 1000 results returned per call - so,
        # with 1500 records per call and 100 records returned for each call, we are looking at 15 requests.
        # Since those requests should be very fast (after all, we're mocking responses), the throttling mechanism
        # should ensure that the call takes at least 3 seconds
        min_expected_call_in_sec = 3
        start_time = timeit.default_timer()

        # call API
        employees = api.find_all()
        time_elapsed = timeit.default_timer() - start_time

        assert employees
        assert len(employees) == 1500
        assert time_elapsed >= min_expected_call_in_sec

        assert httpretty.last_request().headers['Accept'] == 'application/json'
        assert len(httpretty.last_request().querystring['version']) == 1
        assert int(httpretty.last_request().querystring['version'][0]) == 5

        for i in range(10):
            employee_id = random.randint(0, 1500)
            employee = employees[employee_id]
            assert employee.FirstName == 'Lester{0}'.format(employee_id)
            assert employee.LastName == 'Tester{0}'.format(employee_id)
            assert employee.Email == 'lester{0}.tester{0}@test.com'.format(employee_id)
            assert employee.ID == str(100 + employee_id)


# noinspection PyShadowingNames
@pytest.mark.parametrize('body, expected_error_message', [
    ('', 'Unknown error occurred'),
    ('{"Description": "Some Description!"}', 'Some Description!')
])
def test_employees_find_all_error_500(api, body, expected_error_message):
    with HttpPrettyHelper():
        httpretty.register_uri(
                httpretty.GET,
                'https://api.dovico.com/Employees/',
                status=500,
                body=body,
                content_type='application/json; charset=utf-8'
        )

        with pytest.raises(DovicoApiError) as err:
            api.find_all()

        assert err.value.status_code == 500
        assert str(err.value) == expected_error_message

        assert httpretty.last_request().headers['Accept'] == 'application/json'
        assert len(httpretty.last_request().querystring['version']) == 1
        assert int(httpretty.last_request().querystring['version'][0]) == 5


# noinspection PyShadowingNames
def test_employees_find_by_id_success(api):
    with HttpPrettyHelper():
        httpretty.register_uri(
                httpretty.GET,
                'https://api.dovico.com/Employees/114/',
                body=__build_find_by_id_response(FIND_ALL_RESPONSE),
                content_type='application/json; charset=utf-8'
        )

        employee = api.find_by_id(114)

        assert employee.ID == '114'
        assert employee.FirstName == 'Lester1'
        assert employee.LastName == 'Tester1'

        assert httpretty.last_request().headers['Accept'] == 'application/json'
        assert len(httpretty.last_request().querystring['version']) == 1
        assert int(httpretty.last_request().querystring['version'][0]) == 5


# noinspection PyShadowingNames
@pytest.mark.parametrize('body, expected_error_message', [
    ('', 'Unknown error occurred'),
    ('{"Description": "Some Description!"}', 'Some Description!')
])
def test_employees_find_by_id_error_500(api, body, expected_error_message):
    with HttpPrettyHelper():
        httpretty.register_uri(
                httpretty.GET,
                'https://api.dovico.com/Employees/5556/',
                status=500,
                body=body,
                content_type='application/json; charset=utf-8'
        )

        with pytest.raises(DovicoApiError) as err:
            api.find_by_id(5556)

        assert err.value.status_code == 500
        assert str(err.value) == expected_error_message

        assert httpretty.last_request().headers['Accept'] == 'application/json'
        assert len(httpretty.last_request().querystring['version']) == 1
        assert int(httpretty.last_request().querystring['version'][0]) == 5


# noinspection PyShadowingNames
def test_employees_find_by_id_error_400(api):
    with HttpPrettyHelper():
        httpretty.register_uri(
                httpretty.GET,
                'https://api.dovico.com/Employees/5556/',
                status=200,
                body='{"Employees":[]}',
                content_type='application/json; charset=utf-8'
        )

        with pytest.raises(DovicoEntityNotFoundError) as err:
            api.find_by_id(5556)

        assert err.value.status_code == 400
        assert str(err.value) == 'Employee ID [5556] not found'

        assert httpretty.last_request().headers['Accept'] == 'application/json'
        assert len(httpretty.last_request().querystring['version']) == 1
        assert int(httpretty.last_request().querystring['version'][0]) == 5


# pretty-printed JSON string
# noinspection SpellCheckingInspection
PRETTY_FIND_ALL_RESPONSE = """
{
    "Employees":[
        {
            "Wage":"0",
            "ChargeChangedDate":"2011-10-19",
            "FirstName":"Lester1",
            "LastName":"Tester1",
            "WageChangedDate":"2011-10-19",
            "SecurityGroup":{
                "GetItemURI":"https://api.dovico.com/SecurityGroups/96/?version=5",
                "ID":"96",
                "Name":"Administrator"
            },
            "Charge":"0",
            "Integrate":"",
            "AltApproval":"F",
            "Email":"lester1.tester1@test.com",
            "WorkDays":".MTWTF.",
            "StartDate":"2008-08-01",
            "EndDate":"2100-12-31",
            "Hours":"7.5",
            "NotificationTime":"F",
            "NotificationRejected":"F",
            "NotificationExpense":"F",
            "SoftwareAccess":"B",
            "WageCurrency":{
                "Symbol":"$",
                "GetItemURI":"N/A",
                "ID":"-1"
            },
            "Team":{
                "GetItemURI":"N/A",
                "ID":"0",
                "Name":"[None]"
            },
            "CustomFields":[

            ],
            "ChargeCurrency":{
                "Symbol":"$",
                "GetItemURI":"N/A",
                "ID":"-1"
            },
            "ID":"114",
            "Number":"",
            "Archive":"F"
        },
        {
            "Wage":"0",
            "ChargeChangedDate":"2011-10-19",
            "FirstName":"Lester2",
            "LastName":"Tester3",
            "WageChangedDate":"2011-10-19",
            "SecurityGroup":{
                "GetItemURI":"https://api.dovico.com/SecurityGroups/95/?version=5",
                "ID":"95",
                "Name":"Time and Expense Entry"
            },
            "Charge":"0",
            "Integrate":"",
            "AltApproval":"F",
            "Email":"lester2.tester2@test.com",
            "WorkDays":".MTWTF.",
            "StartDate":"2011-02-09",
            "EndDate":"2100-12-31",
            "Hours":"8",
            "NotificationTime":"F",
            "NotificationRejected":"F",
            "NotificationExpense":"F",
            "SoftwareAccess":"T",
            "WageCurrency":{
                "Symbol":"$",
                "GetItemURI":"N/A",
                "ID":"-1"
            },
            "Team":{
                "GetItemURI":"https://api.dovico.com/Teams/116/?version=5",
                "ID":"116",
                "Name":"BIG Sub-contractor"
            },
            "CustomFields":[

            ],
            "ChargeCurrency":{
                "Symbol":"$",
                "GetItemURI":"N/A",
                "ID":"-1"
            },
            "ID":"149",
            "Number":"",
            "Archive":"F"
        },
        {
            "Wage":"0",
            "ChargeChangedDate":"2014-09-24",
            "FirstName":"Lester3",
            "LastName":"Tester3",
            "WageChangedDate":"2014-09-24",
            "SecurityGroup":{
                "GetItemURI":"https://api.dovico.com/SecurityGroups/95/?version=5",
                "ID":"95",
                "Name":"Time and Expense Entry"
            },
            "Charge":"0",
            "Integrate":"",
            "AltApproval":"F",
            "Email":"lester3.tester3@test.com",
            "WorkDays":".MTWTF.",
            "StartDate":"2014-09-22",
            "EndDate":"2100-12-31",
            "Hours":"8",
            "NotificationTime":"F",
            "NotificationRejected":"F",
            "NotificationExpense":"F",
            "SoftwareAccess":"T",
            "WageCurrency":{
                "Symbol":"$",
                "GetItemURI":"N/A",
                "ID":"-1"
            },
            "Team":{
                "GetItemURI":"https://api.dovico.com/Teams/102/?version=5",
                "ID":"102",
                "Name":"BIG Employee"
            },
            "CustomFields":[

            ],
            "ChargeCurrency":{
                "Symbol":"$",
                "GetItemURI":"N/A",
                "ID":"-1"
            },
            "ID":"204",
            "Number":"",
            "Archive":"F"
        },
        {
            "Wage":"0",
            "ChargeChangedDate":"2011-10-19",
            "FirstName":"Lester4",
            "LastName":"Tester4",
            "WageChangedDate":"2011-10-19",
            "SecurityGroup":{
                "GetItemURI":"https://api.dovico.com/SecurityGroups/95/?version=5",
                "ID":"95",
                "Name":"Time and Expense Entry"
            },
            "Charge":"0",
            "Integrate":"",
            "AltApproval":"F",
            "Email":"lester4.tester4@test.com",
            "WorkDays":".MTWTF.",
            "StartDate":"2010-02-08",
            "EndDate":"2011-12-31",
            "Hours":"7.5",
            "NotificationTime":"F",
            "NotificationRejected":"F",
            "NotificationExpense":"F",
            "SoftwareAccess":"N",
            "WageCurrency":{
                "Symbol":"$",
                "GetItemURI":"N/A",
                "ID":"-1"
            },
            "Team":{
                "GetItemURI":"https://api.dovico.com/Teams/115/?version=5",
                "ID":"115",
                "Name":"Inactive"
            },
            "CustomFields":[

            ],
            "ChargeCurrency":{
                "Symbol":"$",
                "GetItemURI":"N/A",
                "ID":"-1"
            },
            "ID":"132",
            "Number":"",
            "Archive":"F"
        },
        {
            "Wage":"0",
            "ChargeChangedDate":"2012-08-29",
            "FirstName":"Lester5",
            "LastName":"Tester5",
            "WageChangedDate":"2012-08-29",
            "SecurityGroup":{
                "GetItemURI":"https://api.dovico.com/SecurityGroups/95/?version=5",
                "ID":"95",
                "Name":"Time and Expense Entry"
            },
            "Charge":"0",
            "Integrate":"",
            "AltApproval":"F",
            "Email":"lester5.tester5@test.com",
            "WorkDays":".MTWTF.",
            "StartDate":"2012-08-29",
            "EndDate":"2100-12-31",
            "Hours":"8",
            "NotificationTime":"F",
            "NotificationRejected":"F",
            "NotificationExpense":"F",
            "SoftwareAccess":"N",
            "WageCurrency":{
                "Symbol":"$",
                "GetItemURI":"N/A",
                "ID":"-1"
            },
            "Team":{
                "GetItemURI":"https://api.dovico.com/Teams/115/?version=5",
                "ID":"115",
                "Name":"Inactive"
            },
            "CustomFields":[

            ],
            "ChargeCurrency":{
                "Symbol":"$",
                "GetItemURI":"N/A",
                "ID":"-1"
            },
            "ID":"181",
            "Number":"",
            "Archive":"T"
        }
    ],
    "PrevPageURI":"N/A",
    "NextPageURI":"N/A"
}
"""

# compact JSON string
FIND_ALL_RESPONSE = json.dumps(json.loads(PRETTY_FIND_ALL_RESPONSE), separators=(',', ':'))
