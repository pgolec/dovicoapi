"""
Unit tests for the Projects
"""
import json
import pytest
import httpretty

# noinspection PyUnresolvedReferences
from tests.test_helpers import credentials, HttpPrettyHelper
from dovicoapi.projects import ProjectsApi


# noinspection PyProtectedMember,PyShadowingNames
def test_projects_entity_descriptor(credentials):
    api = ProjectsApi(credentials.consumer_secret, credentials.data_access_token)
    assert api._entity_name == 'Project'
    assert api._collection_root == 'Projects'
    assert api._collection_path == 'Projects'
    assert api._entity_not_found_message == 'Project ID [{0}] not found'


# noinspection PyShadowingNames
@pytest.fixture(scope='module')
def api(credentials):
    return ProjectsApi(credentials.consumer_secret, credentials.data_access_token)


# noinspection PyShadowingNames
def test_projects_find_all_success(api):
    with HttpPrettyHelper():
        httpretty.register_uri(
                httpretty.GET,
                'https://api.dovico.com/Projects/',
                body=ACTIVE_PROJECTS,
                content_type='application/json; charset=utf-8'
        )

        projects = api.find_all()

        assert projects
        assert len(projects) == 3
        assert projects[0].ID == '108'
        assert projects[0].Status == 'A'
        assert projects[0].Client.Name == 'Client1'
        assert projects[0].Leader.Name == 'Tester1, Lester1'

        assert httpretty.last_request().headers['Accept'] == 'application/json'
        assert len(httpretty.last_request().querystring['version']) == 1
        assert int(httpretty.last_request().querystring['version'][0]) == 5


# noinspection PyShadowingNames
def test_projects_find_active_projects(api):
    with HttpPrettyHelper():
        httpretty.register_uri(
                httpretty.GET,
                'https://api.dovico.com/Projects/',
                body=ACTIVE_PROJECTS,
                content_type='application/json; charset=utf-8',
                cache_control='private',
                date='Tue, 05 Jan 2016 22:33:27 GMT',
                server='Microsoft-IIS/8.0',
                x_aspnet_version='4.0.30319',
                x_powered_by='ASP.NET'
        )

        projects = api.find_active_projects()

        assert projects
        assert len(projects) == 3
        assert projects[0].ID == '108'
        assert projects[0].Status == 'A'
        assert projects[0].Client.Name == 'Client1'
        assert projects[0].Leader.Name == 'Tester1, Lester1'

        assert httpretty.last_request().headers['Accept'] == 'application/json'
        assert len(httpretty.last_request().querystring['version']) == 1
        assert int(httpretty.last_request().querystring['version'][0]) == 5
        assert len(httpretty.last_request().querystring['$filter']) == 1
        assert httpretty.last_request().querystring['$filter'][0] == 'Status eq A'


# pretty-printed JSON string
# noinspection SpellCheckingInspection
PRETTY_ACTIVE_PROJECTS = """
{
    "Projects":[
        {
            "FixedCosts":[

            ],
            "Linked":"F",
            "ExpensesBillableByDefault":"T",
            "PreventEntries":"F",
            "Integrate":"",
            "Leader":{
                "GetItemURI":"https://api.dovico.com/Employees/107/?version=5",
                "ID":"107",
                "Name":"Tester1, Lester1"
            },
            "Status":"A",
            "StartDate":"2008-04-01",
            "EndDate":"2009-03-31",
            "Description":"",
            "BillingBy":"A",
            "TimeBillableByDefault":"T",
            "Name":"Application Maintenance & Support",
            "Client":{
                "GetItemURI":"https://api.dovico.com/Clients/103/?version=5",
                "ID":"103",
                "Name":"Client1"
            },
            "CustomFields":[

            ],
            "ID":"108",
            "MSPConfig":"",
            "BudgetRateDate":"2008-04-01",
            "HideTasks":"T",
            "Currency":{
                "Symbol":"$",
                "GetItemURI":"N/A",
                "ID":"-1"
            },
            "Archive":"F",
            "ProjectGroup":{
                "GetItemURI":"N/A",
                "ID":"0",
                "Name":"[None]"
            },
            "RSProject":"F"
        },
        {
            "FixedCosts":[

            ],
            "Linked":"F",
            "ExpensesBillableByDefault":"T",
            "PreventEntries":"F",
            "Integrate":"<INT><QBD LISTID='1420000-1217612788' /></INT>",
            "Leader":{
                "GetItemURI":"https://api.dovico.com/Employees/102/?version=5",
                "ID":"102",
                "Name":"Tester2, Lester2"
            },
            "Status":"A",
            "StartDate":"2008-07-01",
            "EndDate":"2009-02-28",
            "Description":"",
            "BillingBy":"E",
            "TimeBillableByDefault":"T",
            "Name":"BPM Analysis - Maintenance Mgmt 2008",
            "Client":{
                "GetItemURI":"https://api.dovico.com/Clients/138/?version=5",
                "ID":"138",
                "Name":"Client2"
            },
            "CustomFields":[

            ],
            "ID":"120",
            "MSPConfig":"",
            "BudgetRateDate":"2008-07-01",
            "HideTasks":"F",
            "Currency":{
                "Symbol":"$",
                "GetItemURI":"N/A",
                "ID":"-1"
            },
            "Archive":"F",
            "ProjectGroup":{
                "GetItemURI":"N/A",
                "ID":"0",
                "Name":"[None]"
            },
            "RSProject":"F"
        },
        {
            "FixedCosts":[

            ],
            "Linked":"F",
            "ExpensesBillableByDefault":"T",
            "PreventEntries":"F",
            "Integrate":"",
            "Leader":{
                "GetItemURI":"https://api.dovico.com/Employees/102/?version=5",
                "ID":"102",
                "Name":"Tester2, Lester2"
            },
            "Status":"A",
            "StartDate":"2010-12-13",
            "EndDate":"2012-02-01",
            "Description":"",
            "BillingBy":"A",
            "TimeBillableByDefault":"T",
            "Name":"C3P (DH)",
            "Client":{
                "GetItemURI":"https://api.dovico.com/Clients/180/?version=5",
                "ID":"180",
                "Name":"Client3"
            },
            "CustomFields":[

            ],
            "ID":"364",
            "MSPConfig":"",
            "BudgetRateDate":"2010-12-13",
            "HideTasks":"F",
            "Currency":{
                "Symbol":"$",
                "GetItemURI":"N/A",
                "ID":"-1"
            },
            "Archive":"F",
            "ProjectGroup":{
                "GetItemURI":"N/A",
                "ID":"0",
                "Name":"[None]"
            },
            "RSProject":"T"
        }
    ]
}
"""

# compact JSON string
ACTIVE_PROJECTS = json.dumps(json.loads(PRETTY_ACTIVE_PROJECTS), separators=(',', ':'))
