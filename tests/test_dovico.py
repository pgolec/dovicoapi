"""
Unit tests for the Dovico class.
"""
import pytest
# noinspection PyUnresolvedReferences
from tests.test_helpers import credentials
from dovicoapi import Dovico
from dovicoapi.assignments import AssignmentsApi
from dovicoapi.clients import ClientsApi
from dovicoapi.currencies import CurrenciesApi, CurrencyRateApi
from dovicoapi.custom import CustomFieldTemplatesApi, CustomTerminologyApi
from dovicoapi.employees import EmployeesApi
from dovicoapi.expenses import ExpenseCategoriesApi, ExpenseEntriesApi
from dovicoapi.projects import ProjectsApi
from dovicoapi.security import SecurityGroupsApi
from dovicoapi.sheets import SheetsApi
from dovicoapi.tasks import TasksApi
from dovicoapi.teams import TeamsApi
from dovicoapi.timeentries import TimeEntriesApi


# noinspection PyShadowingNames
@pytest.fixture
def dovico(credentials):
    return Dovico(credentials.consumer_secret, credentials.data_access_token)


# noinspection PyShadowingNames
def test_assignments(dovico):
    assert dovico.assignments is not None
    assert isinstance(dovico.assignments, AssignmentsApi)


# noinspection PyShadowingNames
def test_clients(dovico):
    assert dovico.clients is not None
    assert isinstance(dovico.clients, ClientsApi)


# noinspection PyShadowingNames
def test_currencies(dovico):
    assert dovico.currencies is not None
    assert isinstance(dovico.currencies, CurrenciesApi)


# noinspection PyShadowingNames
def test_currency_rate(dovico):
    assert dovico.currency_rate is not None
    assert isinstance(dovico.currency_rate, CurrencyRateApi)


# noinspection PyShadowingNames
def test_custom_field_templates(dovico):
    assert dovico.custom_field_templates is not None
    assert isinstance(dovico.custom_field_templates, CustomFieldTemplatesApi)


# noinspection PyShadowingNames
def test_custom_terminology(dovico):
    assert dovico.custom_terminology is not None
    assert isinstance(dovico.custom_terminology, CustomTerminologyApi)


# noinspection PyShadowingNames
def test_employees(dovico):
    assert dovico.employees is not None
    assert isinstance(dovico.employees, EmployeesApi)


# noinspection PyShadowingNames
def test_expense_categories(dovico):
    assert dovico.expense_categories is not None
    assert isinstance(dovico.expense_categories, ExpenseCategoriesApi)


# noinspection PyShadowingNames
def test_expense_entries(dovico):
    assert dovico.expense_entries is not None
    assert isinstance(dovico.expense_entries, ExpenseEntriesApi)


# noinspection PyShadowingNames
def test_projects(dovico):
    assert dovico.projects is not None
    assert isinstance(dovico.projects, ProjectsApi)


# noinspection PyShadowingNames
def test_security_groups(dovico):
    assert dovico.security_groups is not None
    assert isinstance(dovico.security_groups, SecurityGroupsApi)


# noinspection PyShadowingNames
def test_sheets(dovico):
    assert dovico.sheets is not None
    assert isinstance(dovico.sheets, SheetsApi)


# noinspection PyShadowingNames
def test_tasks(dovico):
    assert dovico.tasks is not None
    assert isinstance(dovico.tasks, TasksApi)


# noinspection PyShadowingNames
def test_teams(dovico):
    assert dovico.teams is not None
    assert isinstance(dovico.teams, TeamsApi)


# noinspection PyShadowingNames
def test_time_entries(dovico):
    assert dovico.time_entries is not None
    assert isinstance(dovico.time_entries, TimeEntriesApi)
