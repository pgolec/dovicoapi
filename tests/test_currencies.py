"""
Unit tests for the Currency and Currency Rate
"""
# noinspection PyUnresolvedReferences
from tests.test_helpers import credentials
from dovicoapi.currencies import CurrenciesApi


# noinspection PyProtectedMember,PyShadowingNames
def test_currencies_entity_descriptor(credentials):
    api = CurrenciesApi(credentials.consumer_secret, credentials.data_access_token)
    assert api._entity_name == 'Currency'
    assert api._collection_root == 'Currencies'
    assert api._collection_path == 'Currencies'
    assert api._entity_not_found_message == 'Currency ID [{0}] not found'
