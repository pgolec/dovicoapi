"""
Unit tests for the base module in dovicoapi package
"""
from datetime import datetime
import pytest
from dovicoapi.base import (
    DovicoError,
    DovicoApiError,
    DovicoUnauthorizedError,
    DovicoEntityNotFoundError,
    DovicoResourceHandlerApi,
    DovicoEntityDescriptor
)


def test_throwing_cloud_hub_error():
    msg = 'Testing It'
    with pytest.raises(DovicoError) as err:
        raise DovicoError(msg)

    assert str(err.value) == msg


def test_throwing_dovico_api_error():
    msg = 'Testing It'
    status = 555
    with pytest.raises(DovicoApiError) as err:
        raise DovicoApiError(msg, status)

    assert str(err.value) == msg
    assert err.value.status_code == status


def test_throwing_dovico_unauthorized_error_with_explicit_args():
    msg = 'Testing It'
    status = 555
    with pytest.raises(DovicoUnauthorizedError) as err:
        raise DovicoUnauthorizedError(msg, status)

    assert str(err.value) == msg
    assert err.value.status_code == status


def test_throwing_dovico_unauthorized_error_with_implicit_args():
    with pytest.raises(DovicoUnauthorizedError) as err:
        raise DovicoUnauthorizedError()

    assert str(err.value) == DovicoUnauthorizedError.DEFAULT_MESSAGE
    assert err.value.status_code == 401


@pytest.mark.parametrize('message,entity_id,status,expected_message,expected_status', [
        ('Testing It', '2435', 400, 'Testing It', 400),
        ('Something {0}', 111, 356, 'Something 111', 356)
    ])
def test_throwing_dovico_entity_not_found_error_with_explicit_args(
        message,
        entity_id,
        status,
        expected_message,
        expected_status
):
    with pytest.raises(DovicoEntityNotFoundError) as err:
        raise DovicoEntityNotFoundError(entity_id, message=message, status_code=status)

    assert str(err.value) == expected_message
    assert err.value.status_code == expected_status


def test_throwing_dovico_entity_not_found_error_with_implicit_args():
    entity_id = 555
    with pytest.raises(DovicoEntityNotFoundError) as err:
        raise DovicoEntityNotFoundError(entity_id)

    assert str(err.value) == DovicoEntityNotFoundError.DEFAULT_MESSAGE.format(entity_id)
    assert err.value.status_code == 400


# noinspection PyProtectedMember
@pytest.mark.parametrize('query_filter,date_range,expected_params', [
        ('Status eq 3', ('2015-10-01', '2015-10-31'), {'$filter': 'Status eq 3', 'daterange': '2015-10-01 2015-10-31'}),
        ('AA eq 3', ['2014-11-01', '2015-11-15'], {'$filter': 'AA eq 3', 'daterange': '2014-11-01 2015-11-15'}),
        ('AA eq 3', ['2014-11-01', '2015-11-15'], {'$filter': 'AA eq 3', 'daterange': '2014-11-01 2015-11-15'}),
        (
            'BB eq 5',
            [datetime.strptime('11-01-2014', '%m-%d-%Y'), datetime.strptime('15-11-2015', '%d-%m-%Y')],
            {'$filter': 'BB eq 5', 'daterange': '2014-11-01 2015-11-15'}
        ),
        (
            'CC eq 8',
            (datetime.strptime('10-01-2015', '%m-%d-%Y'), datetime.strptime('31-10-2015', '%d-%m-%Y')),
            {'$filter': 'CC eq 8', 'daterange': '2015-10-01 2015-10-31'}
        )
    ])
def test_extract_params(query_filter, date_range, expected_params):
    t = DovicoResourceHandlerApi('aa', 'bb')
    params = t._extract_params(query_filter=query_filter, date_range=date_range)
    assert params == expected_params


class TestDovicoEntityDescriptor(object):
    """
    DovicoEntityDescriptor unit tests
    """
    @pytest.mark.parametrize('name, root, path', [
        ('Test Name', 'Test Root', 'Test Path'),
        ('Employee', 'Employees', 'Employees'),
        ('Project', 'Project', 'UUU'),
    ])
    def test_explicit_args(self, name, root, path):
        d = DovicoEntityDescriptor(name, root, path)
        assert d.name == name
        assert d.collection_root == root
        assert d.collection_path == path

    @pytest.mark.parametrize('name, expected_root, expected_path', [
        ('Test Name', 'Test Names', 'Test Names'),
        ('Employee', 'Employees', 'Employees'),
        ('Project', 'Projects', 'Projects'),
        ('Asses', 'Assess', 'Assess'),
    ])
    def test_single_name(self, name, expected_root, expected_path):
        d = DovicoEntityDescriptor(name)
        assert d.name == name
        assert d.collection_root == expected_root
        assert d.collection_path == expected_path

    @pytest.mark.parametrize('name, root, path, expected_root, expected_path', [
        ('Test Name', None, 'AAA', 'Test Names', 'AAA'),
        ('Employee', 'TOTS', 'AAA', 'TOTS', 'AAA'),
        ('Employee', 'TOTS', None, 'TOTS', 'TOTS'),
        ('Employee', None, None, 'Employees', 'Employees'),
        ('Employee', '', None, 'Employees', 'Employees'),
        ('Employee', '', '', 'Employees', 'Employees'),
        ('Employee', None, '', 'Employees', 'Employees'),
    ])
    def test_mixed_args(self, name, root, path, expected_root, expected_path):
        d = DovicoEntityDescriptor(name, root, path)
        assert d.name == name
        assert d.collection_root == expected_root
        assert d.collection_path == expected_path

    @pytest.mark.parametrize('name', [
        '', None, '  ', '  \t', '  \t\n  ', '\n  \t', '\t\n\t  '
    ])
    def test_name_required(self, name):
        with pytest.raises(ValueError):
            DovicoEntityDescriptor(name)
